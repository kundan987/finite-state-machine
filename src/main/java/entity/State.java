package entity;

public class State {

    private String name;

    private boolean isEnd;

    public State(String name, boolean isEnd) {
        this.name = name;
        this.isEnd = isEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }
}
