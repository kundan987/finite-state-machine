package entity;

import java.util.List;

public class Consumer {
    private int id;
    private String name;
    private List<State> interestedStates;

    public Consumer(int id, String name, List<State> interestedStates) {
        this.id = id;
        this.name = name;
        this.interestedStates = interestedStates;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<State> getInterestedStates() {
        return interestedStates;
    }

    public void setInterestedStates(List<State> interestedStates) {
        this.interestedStates = interestedStates;
    }
}
