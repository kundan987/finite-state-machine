package request;

import entity.Action;
import entity.Event;
import entity.Producer;
import entity.State;

public class EventRequest {

    private Event event;
    private Action action;
    private Producer producer;
    private State startState;

    public EventRequest(Event event, Action action, Producer producer, State startState) {
        this.event = event;
        this.action = action;
        this.producer = producer;
        this.startState = startState;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public State getStartState() {
        return startState;
    }

    public void setStartState(State startState) {
        this.startState = startState;
    }
}
