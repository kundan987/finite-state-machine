package service;

import entity.Consumer;
import entity.Event;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ConsumerService{
    LinkedList<Event> requestList = new LinkedList<>();
    Map<Integer, List<Consumer>> consumerMap;

    public void publish(){
        while (true){
            synchronized (this){
                if(requestList.isEmpty()) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Event event = requestList.removeFirst();
                if(consumerMap.containsKey(event.getId())){
                    List<Consumer> consumers = consumerMap.get(event.getId());
                    for (Consumer consumer : consumers){
                        if(consumer.getInterestedStates().contains(event.getCurrentState()))
                            System.out.println(Thread.currentThread().getName() + " consumer - " + consumer.getId() + " : " + event.getCurrentState().getName() + " received");
                    }
                } else {
                    System.out.println("No Consumer found");
                }
            }
        }

    }

    public void publishToQueue(Event request){
        requestList.add(request);
    }

    public void registerConsumer(Map<Integer, List<Consumer>> consumerMap){
        this.consumerMap = consumerMap;
    }
}
