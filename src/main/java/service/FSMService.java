package service;

import entity.*;
import request.EventRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FSMService {

    Map<State, List<State>> transitionMap;
    Map<Integer, Event> eventDataMap;
    Map<Integer, Producer> producerMap;

    Map<Action, List<State>> actionStatesMap;

    ConsumerService consumerService;



    public FSMService(Map<State, List<State>> transitionMap, Map<Integer, Producer> producerMap, Map<Integer, List<Consumer>> consumerMap, Map<Action, List<State>> actionStatesMap) {
        this.transitionMap = transitionMap;
        this.eventDataMap = new HashMap<>();
        this.producerMap = producerMap;
        this.actionStatesMap = actionStatesMap;
        this.consumerService = new ConsumerService();
        this.consumerService.registerConsumer(consumerMap);

    }

    public void pushEvent(EventRequest request){
        if (producerMap.containsKey(request.getEvent().getId())){
            Event event = null;
            boolean flag = false;
            if (eventDataMap.containsKey(request.getEvent().getId())){
                event = eventDataMap.get(request.getEvent().getId());
                List<State> allowedTransition = transitionMap.get(event.getCurrentState());
                List<State> states = actionStatesMap.get(request.getAction());
                for(State toState : states){
                    if (allowedTransition.contains(toState)){
                        event.setCurrentState(toState);
                        flag = true;
                    }
                }

            } else if(request.getAction().getName().equals("fullfillOrder")){
                event = new Event(request.getEvent().getId());
                event.setCurrentState(request.getStartState());
                flag = true;
            }
            if(flag){
                eventDataMap.put(event.getId(), event);
                consumerService.publishToQueue(event);

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        consumerService.publish();
                    }
                });

                thread.start();
            } else  {
                System.out.println("Transition not allowed");
            }

        } else {
            System.out.println("Producer not registered");
        }
    }


}
