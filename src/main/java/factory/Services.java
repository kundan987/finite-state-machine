package factory;

import service.IConsumerService;
import service.IFSMService;

public interface Services {
    IConsumerService getConsumerService();

    IFSMService getFSMService();

}
