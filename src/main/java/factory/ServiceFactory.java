package factory;

import memory.ConsumerMemoryService;
import memory.FSMConsumerService;
import service.IConsumerService;
import service.IFSMService;

public class ServiceFactory implements Services{

    IConsumerService consumerService;
    IFSMService fsmService;

    public ServiceFactory() {
        this.consumerService = new ConsumerMemoryService();
        this.fsmService = new FSMConsumerService();
    }

    @Override
    public IConsumerService getConsumerService() {
        return consumerService;
    }

    @Override
    public IFSMService getFSMService() {
        return fsmService;
    }

}
