import entity.*;
import request.EventRequest;
import service.FSMService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        State created = new State("CREATED", false);
        State activated = new State("ACTIVATED", false);
        State cancelled = new State("CANCELLED", true);
        State inprogress = new State("INPROGRESS", false);
        State completed = new State("COMPLETED", true);

        Action fullfillOrder = new Action("fullfillOrder");
        Action activate = new Action("activate");
        Action cancel = new Action("cancel");
        Action makeProgress = new Action("makeProgress");
        Action complete = new Action("complete");

        Map<Action, List<State>> actionStatesMap = new HashMap<>();

        actionStatesMap.put(fullfillOrder, Arrays.asList(created));
        actionStatesMap.put(activate, Arrays.asList(activated));
        actionStatesMap.put(cancel, Arrays.asList(cancelled));
        actionStatesMap.put(makeProgress, Arrays.asList(inprogress));
        actionStatesMap.put(complete, Arrays.asList(completed));

        Map<State, List<State>> transitionMap = new HashMap<>();

        transitionMap.put(created, Arrays.asList(activated, cancelled));
        transitionMap.put(activated, Arrays.asList(inprogress));
        transitionMap.put(inprogress, Arrays.asList(cancelled, completed));

        Producer producer = new Producer(1, "producer");


        Map<Integer, Producer> producerMap = new HashMap<>();
        producerMap.put(1, producer);

        Consumer consumer1 = new Consumer(1, "consumer1", Arrays.asList(created, activated, inprogress));
        Consumer consumer2 = new Consumer(2, "consumer2", Arrays.asList(created, cancelled, completed));
        Map<Integer, List<Consumer>> consumerMap = new HashMap<>();
        consumerMap.put(1, Arrays.asList(consumer1, consumer2));

        FSMService service = new FSMService(transitionMap, producerMap, consumerMap, actionStatesMap);

        Event event = new Event(1);
        EventRequest request = new EventRequest(event, fullfillOrder, producer, created);
        service.pushEvent(request);

        request = new EventRequest(event, activate, producer, created);
        service.pushEvent(request);

        request = new EventRequest(event, makeProgress, producer, created);
        service.pushEvent(request);


        request = new EventRequest(event, complete, producer, created);
        service.pushEvent(request);
    }
}
